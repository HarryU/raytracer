use nalgebra::Vector3;
use scene::{Color, Element, Intersection, Scene, SurfaceType};
use std::f32;
use std::f32::consts::PI;

#[derive(Debug)]
pub struct Ray {
    pub origin: Vector3<f32>,
    pub direction: Vector3<f32>,
}

impl Ray {
    pub fn create_prime(x: f32, y: f32, scene: &Scene) -> Ray {
        assert!(scene.width > scene.height);
        let fov_adjustment = fov_factor(&scene.fov);
        let aspect_ratio = (scene.width as f64) / (scene.height as f64);

        let normalized_device_coord_x = (x as f64 + 0.5) / scene.width as f64;
        let normalized_device_coord_y = (y as f64 + 0.5) / scene.height as f64;

        let screen_coord_x = normalized_device_coord_x * 2.0 - 1.0;
        let screen_coord_y = 1.0 - normalized_device_coord_y * 2.0;

        let sensor_x = (screen_coord_x * aspect_ratio) * fov_adjustment;
        let sensor_y = screen_coord_y * fov_adjustment;

        let direction = Vector3::new(sensor_x as f32, sensor_y as f32, -1.0);

        Ray {
            origin: scene.camera.position,
            direction: (scene.camera.rotation_matrix * direction).normalize(),
        }
    }

    pub fn create_reflection(
        surface_normal: Vector3<f32>,
        incident_direction: Vector3<f32>,
        surface_intersection: Vector3<f32>,
        shadow_bias: f64,
    ) -> Ray {
        Ray {
            origin: surface_intersection + (surface_normal.scale(shadow_bias as f32)),
            direction: incident_direction
                - (2.0 * incident_direction.dot(&surface_normal) * surface_normal),
        }
    }

    pub fn create_refraction(
        normal: Vector3<f32>,
        incident: Vector3<f32>,
        surface_intersection: Vector3<f32>,
        shadow_bias: f64,
        index: f32,
    ) -> Option<Ray> {
        let mut ref_normal = normal;
        let mut eta_t = index as f64;
        let mut eta_i = 1.0f64;
        let mut i_dot_n = normal
            .normalize()
            .dot(&incident.normalize())
            .min(1.0)
            .max(-1.0);
        if i_dot_n < 0.0 {
            // ray is entering object
            i_dot_n = -i_dot_n;
        } else {
            // ray is in the same direction as normal therefore leaving object so we swap refractive indices
            ref_normal = -normal;
            eta_i = eta_t;
            eta_t = 1.0f64;
        }

        let eta = (eta_i / eta_t) as f32;
        let k = 1.0 - (eta * eta) * (1.0 - i_dot_n * i_dot_n);
        if k < 0.0 {
            None
        } else {
            Some(Ray {
                origin: surface_intersection + (ref_normal.scale(-shadow_bias as f32)),
                direction: ((incident - ref_normal * i_dot_n).scale(eta)
                    - ref_normal.scale(k.sqrt()))
                .normalize(),
            })
        }
    }
}

fn fov_factor(fov: &f64) -> f64 {
    (fov.to_radians() / 2.0).tan()
}

pub struct TextureCoords {
    pub x: f32,
    pub y: f32,
}

pub trait Intersectable {
    fn intersect(&self, ray: &Ray) -> Option<f64>;
    fn surface_normal(&self, hit_point: &Vector3<f32>) -> Vector3<f32>;
    fn texture_coords(&self, hit_point: &Vector3<f32>) -> TextureCoords;
}

impl Intersectable for Element {
    fn intersect(&self, ray: &Ray) -> Option<f64> {
        match *self {
            Element::Sphere(ref s) => s.intersect(ray),
            Element::Plane(ref p) => p.intersect(ray),
            Element::Disk(ref d) => d.intersect(ray),
        }
    }

    fn surface_normal(&self, hit_point: &Vector3<f32>) -> Vector3<f32> {
        match *self {
            Element::Sphere(ref s) => s.surface_normal(hit_point),
            Element::Plane(ref p) => p.surface_normal(hit_point),
            Element::Disk(ref d) => d.surface_normal(hit_point),
        }
    }

    fn texture_coords(&self, hit_point: &Vector3<f32>) -> TextureCoords {
        match *self {
            Element::Sphere(ref s) => s.texture_coords(hit_point),
            Element::Plane(ref p) => p.texture_coords(hit_point),
            Element::Disk(ref d) => d.texture_coords(hit_point),
        }
    }
}

pub fn cast_ray(scene: &Scene, ray: &Ray, depth: u32) -> Color {
    if depth >= scene.max_recursion_depth {
        return Color::black();
    }

    let intersection = scene.trace(&ray);
    intersection
        .map(|i| get_color(&scene, &ray, &i, depth))
        .unwrap_or(Color::black())
}

fn get_color(scene: &Scene, ray: &Ray, intersection: &Intersection, depth: u32) -> Color {
    let hit_point = ray.origin + (ray.direction.scale(intersection.distance as f32));
    let surface_normal = intersection.element.surface_normal(&hit_point);

    let material = intersection.element.material();
    match material.surface {
        SurfaceType::Diffuse => {
            diffuse_color(scene, intersection.element, hit_point, surface_normal)
        }
        SurfaceType::Reflective { reflectivity } => {
            let mut color = diffuse_color(scene, intersection.element, hit_point, surface_normal);
            let reflection_ray =
                Ray::create_reflection(surface_normal, ray.direction, hit_point, scene.shadow_bias);
            color = color * (1.0 - reflectivity);
            color = color + (cast_ray(scene, &reflection_ray, depth + 1) * reflectivity);
            color.clamp()
        }
        SurfaceType::Refractive { index } => {
            let fraction_reflected = fresnel(surface_normal, ray.direction, index);

            let reflection_ray =
                Ray::create_reflection(surface_normal, ray.direction, hit_point, scene.shadow_bias);
            let reflection_color = cast_ray(scene, &reflection_ray, depth + 1);

            let refraction_color = if fraction_reflected < 1.0 {
                let refraction_ray = Ray::create_refraction(
                    surface_normal,
                    ray.direction,
                    hit_point,
                    scene.shadow_bias,
                    index,
                )
                .unwrap();
                cast_ray(scene, &refraction_ray, depth + 1)
            } else {
                Color::black()
            };
            let color = reflection_color * fraction_reflected
                + refraction_color * (1.0 - fraction_reflected);
            color.clamp()
        }
    }
}

fn diffuse_color(
    scene: &Scene,
    element: &Element,
    hit_point: Vector3<f32>,
    surface_normal: Vector3<f32>,
) -> Color {
    let mut color = Color::black();
    for light in &scene.lights {
        let direction_to_light = light.direction_from(&hit_point);
        let shadow_ray = Ray {
            origin: hit_point + direction_to_light.scale(scene.shadow_bias as f32),
            direction: direction_to_light,
        };
        let shadow_intersection = scene.trace(&shadow_ray);
        let in_light = shadow_intersection.is_none()
            || shadow_intersection.unwrap().distance > light.distance(&hit_point);
        let light_intensity = if in_light {
            light.intensity(&hit_point)
        } else {
            0.0
        };
        let light_power =
            (surface_normal.dot(&direction_to_light) as f32).max(0.0) * light_intensity;
        let light_reflected = element.albedo() / PI;
        let light_color = light.color() * light_power * light_reflected;
        color = color + (element.color(&hit_point) * light_color);
    }
    color.clamp()
}

fn fresnel(incident: Vector3<f32>, normal: Vector3<f32>, index: f32) -> f64 {
    let i_dot_n = incident.dot(&normal) as f64;
    let mut eta_i = 1.0;
    let mut eta_t = index as f64;
    if i_dot_n > 0.0 {
        eta_i = eta_t;
        eta_t = 1.0;
    }

    let sin_t = eta_i / eta_t * (1.0 - i_dot_n * i_dot_n).max(0.0).sqrt();
    if sin_t > 1.0 {
        return 1.0;
    } else {
        let cos_t = (1.0 - sin_t * sin_t).max(0.0).sqrt();
        let cos_i = cos_t.abs();
        let r_s = ((eta_t * cos_i) - (eta_i * cos_t)) / ((eta_t * cos_i) + (eta_i * cos_t));
        let r_p = ((eta_i * cos_i) - (eta_t * cos_t)) / ((eta_i * cos_i) + (eta_t * cos_t));
        return (r_s * r_s + r_p * r_p) / 2.0;
    }
}
