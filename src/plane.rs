use nalgebra::Vector3;
use rendering::{Intersectable, Ray, TextureCoords};
use scene::{Color, Coloration, Material, SurfaceType};

#[derive(Serialize, Deserialize, Debug)]
pub struct Plane {
    pub origin: Vector3<f32>,
    // normal should be normalised on deserialisation
    pub normal: Vector3<f32>,
    pub material: Material,
}

impl Default for Plane {
    fn default() -> Plane {
        Plane {
            origin: Vector3::new(0.0, 0.0, -20.0),
            normal: Vector3::new(0.0, 0.0, -1.0),
            material: Material {
                coloration: Coloration::Color(Color {
                    red: 0.4,
                    green: 0.5,
                    blue: 0.65,
                }),
                albedo: 0.23,
                surface: SurfaceType::Reflective { reflectivity: 0.3 },
            },
        }
    }
}

impl Intersectable for Plane {
    fn intersect(&self, ray: &Ray) -> Option<f64> {
        let normal = &self.normal;
        let denom = normal.dot(&ray.direction);
        if denom > 1e-6 {
            let v = self.origin - ray.origin;
            let distance = v.dot(&normal) / denom;
            if distance >= 0.0 {
                return Some(distance as f64);
            }
        }
        None
    }

    fn surface_normal(&self, _: &Vector3<f32>) -> Vector3<f32> {
        -self.normal
    }

    fn texture_coords(&self, hit_point: &Vector3<f32>) -> TextureCoords {
        let mut x_axis = self.normal.cross(&Vector3::new(0.0, 0.0, 1.0));
        if x_axis.norm() == 0.0 {
            x_axis = self.normal.cross(&Vector3::new(0.0, 0.0, 1.0));
        }
        let y_axis = self.normal.cross(&x_axis);
        let hit_vec = *hit_point - self.origin;

        TextureCoords {
            x: hit_vec.dot(&x_axis) as f32,
            y: hit_vec.dot(&y_axis) as f32,
        }
    }
}
