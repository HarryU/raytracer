use nalgebra::Vector3;
use plane::Plane;
use rendering::{Intersectable, Ray, TextureCoords};
use scene::Material;

#[derive(Serialize, Deserialize, Debug)]
pub struct Disk {
    pub origin: Vector3<f32>,
    // normal should be normalised on deserialisation
    pub normal: Vector3<f32>,
    pub radius: f64,
    pub material: Material,
}

impl Intersectable for Disk {
    fn intersect(&self, ray: &Ray) -> Option<f64> {
        let plane = Plane {
            origin: self.origin,
            normal: self.normal,
            material: self.material.clone(),
        };
        let plane_intersection = plane.intersect(ray);
        if plane_intersection.is_some() {
            let v = (ray.origin + ray.direction.scale(plane_intersection.unwrap() as f32))
                - self.origin;
            if v.dot(&v) < (self.radius * self.radius) as f32 {
                plane_intersection
            } else {
                None
            }
        } else {
            None
        }
    }

    fn surface_normal(&self, _: &Vector3<f32>) -> Vector3<f32> {
        -self.normal
    }

    fn texture_coords(&self, hit_point: &Vector3<f32>) -> TextureCoords {
        let mut x_axis = self.normal.cross(&Vector3::new(0.0, 0.0, 1.0));
        if x_axis.norm() == 0.0 {
            x_axis = self.normal.cross(&Vector3::new(0.0, 1.0, 0.0));
        }
        let y_axis = self.normal.cross(&x_axis);
        let hit_vec = *hit_point - self.origin;

        TextureCoords {
            x: hit_vec.dot(&x_axis) as f32,
            y: hit_vec.dot(&y_axis) as f32,
        }
    }
}
