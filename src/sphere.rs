use nalgebra::Vector3;
use rendering::{Intersectable, Ray, TextureCoords};
use scene::{Color, Coloration, Material, SurfaceType};

#[derive(Serialize, Deserialize, Debug)]
pub struct Sphere {
    pub centre: Vector3<f32>,
    pub radius: f64,
    pub material: Material,
}

impl Default for Sphere {
    fn default() -> Sphere {
        Sphere {
            centre: Vector3::new(0.0, -1.8, 0.0),
            radius: 0.2,
            material: Material {
                coloration: Coloration::Color(Color {
                    red: rand::random::<f32>(),
                    blue: rand::random::<f32>(),
                    green: rand::random::<f32>(),
                }),
                albedo: rand::random::<f32>(),
                surface: SurfaceType::Diffuse,
            },
        }
    }
}

impl Intersectable for Sphere {
    fn intersect(&self, ray: &Ray) -> Option<f64> {
        let line: Vector3<f32> = self.centre - ray.origin;

        let adj = line.dot(&ray.direction) as f64;
        let d2 = line.dot(&line) as f64 - (adj * adj);
        let radius2 = self.radius * self.radius as f64;
        if d2 > (radius2) {
            return None;
        }
        let thc = (radius2 - d2).sqrt();
        let t0 = adj - thc;
        let t1 = adj + thc;

        if t0 < 0.0 && t1 < 0.0 {
            return None;
        } else if t0 < 0.0 {
            Some(t1)
        } else if t1 < 0.0 {
            Some(t0)
        } else {
            let distance = if t0 < t1 { t0 } else { t1 };
            Some(distance)
        }
    }

    fn surface_normal(&self, hit_point: &Vector3<f32>) -> Vector3<f32> {
        let surface_normal = (hit_point.clone() - self.centre.clone()).normalize();
        surface_normal
    }

    fn texture_coords(&self, hit_point: &Vector3<f32>) -> TextureCoords {
        let hit_vec = *hit_point - self.centre;
        TextureCoords {
            x: (1.0 + (hit_vec.z.atan2(hit_vec.x) as f32) / std::f32::consts::PI) * 0.5,
            y: (hit_vec.y as f64 / self.radius).acos() as f32 / std::f32::consts::PI,
        }
    }
}
